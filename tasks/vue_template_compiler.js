/*
 * grunt-vue-template-compiler
 * https://github.com/simoncast/grunt-vue-template-compiler
 *
 * Copyright (c) 2017 Simon Cast
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  var compiler = require('vue-template-compiler');
  var complier2015 = require('vue-template-es2015-compiler');
  var validate = require('vue-template-validator');
  var path = require('path');

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks

  grunt.registerMultiTask('vue_template_compiler', 'run the npm vue-template-compiler on html files to compile them to render functions to improve FE performance', function() {
    // Merge task-specific and/or target-specific options with these defaults.
    var options = this.options({
      validate: true,
      es2015: true,
      validateOnly: false,
      useModules: false,
      stripWith: true
    });

    var es2015opts = {
      transforms: {
        modules: options.useModules,
        stripWith: options.stripWith
      }
    }

    // Iterate over all specified file groups.
    this.files.forEach(function(f) {
      var src = f.src.filter(function(filepath) {
        // Warn on and remove invalid source files (if nonull was set).
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn('Source file "' + filepath + '" not found.');
          return false;
        } else {
          //return grunt.file.read(filepath);
          return true;
        }
      });

      if(src.length === 0)
      {
        grunt.log.warn('Source file "' +filepath + '" is empty');
        return;
      }
      if((typeof src === "object") || (typeof src === 'array'))
      {
        var failed = false;
        if(options.validate)
        {
          src.forEach(function(f){
            var output;
            var input;
            input =  grunt.file.read(f);
            //validate first
            try {
              var warnings = validate(input);
              if(warnings.length)
              {
                grunt.log.warn('Validation of template '+ f + ' failed \n');
                warnings.forEach(function(msg){
                  grunt.log.warn(msg);
                });
                failed = true;
              }
            } catch(e) {
              console.log(e);
              err = new Error('Template validation failed.');
              if (e.message) {
                err.message += '\n' + e.message + '. \n';
                if (e.line) {
                  err.message += 'Line ' + e.line + ' in ' + f + '\n';
                }
              }
              err.origError = e;
              grunt.log.warn('Vue template validation ' + f+ ' failed.');
              grunt.fail.warn(err);                    
            }
          });
          if(failed)
          {
            grunt.fail.warn('Vue template validation failed');
          } else {
            grunt.log.ok('Vue template validation passed');
          }
        }
        //compile after validation
        if(!options.validateOnly)
        {
          src.forEach(function(f){
            var output;
            var input;
            var output2015;
            var failed = false;
            input =  grunt.file.read(f).replace(/[\s\r\n]+/gm, ' ').trim();
            try {
              grunt.log.writeln('Compiling ' + f + '\n');
              output = compiler.compile(input);

              let statrend = [];
              for(let stat of output.staticRenderFns) {
                statrend.push('function() {' + stat + '}');
              }
              statrend = statrend.join(',');

              let code = '';
              code += complier2015('function render() {' + output.render + '}',es2015opts);
              code += "\n";
              code += complier2015('let staticRenderFns = [' + statrend + '];', es2015opts);
              code += "\n";
              code += 'export {render, staticRenderFns}';

              let filename = f.split('/').pop().split('.')[0];
              let newpath = path.dirname(f) + '/tmp/' + filename + '.js';
              console.log('Writing render file to:', newpath);

              grunt.file.write(newpath, code);
            } catch(e) {
              console.log(e);
              let err = new Error('Template compiling failed.');
              if (e.message) {
                err.message += '\n' + e.message + '. \n';
                if (e.line) {
                  err.message += 'Line ' + e.line + ' in ' + f + '\n';
                }
              }
              err.origError = e;
              grunt.log.warn('Vue template compiling ' + f+ ' failed.');
              grunt.fail.warn(err);        
            }
          });
          if(failed)
          {
            grunt.fail.warn('Templates compiling failed');
          }
        }
      } else {
        var output;
        var input;
        var output2015;
        input =  grunt.file.read(src); 
        if(options.validate)
        {
          try {
            var warnings = validate(input);
            if(warnings.length)
            {
              grunt.log.warn('Validation of template '+ f + 'failed \n');
              warnings.forEach(function(msg){
                grunt.log.warn(msg);
              });
              grunt.fail.warn('Vue template validation failed');
            }
          } catch(e) {
            console.log(e);
            err = new Error('Template validation failed.');
            if (e.message) {
              err.message += '\n' + e.message + '. \n';
              if (e.line) {
                err.message += 'Line ' + e.line + ' in ' + f + '\n';
              }
            }
            err.origError = e;
            grunt.log.warn('Vue template validation ' + f+ ' failed.');
            grunt.fail.warn(err);                    
          }          
        }   
        if(!options.validateOnly) 
        {
          try {
            output = compiler.compileToFunctions(input);
            output2015 = complier2015.transpile('module.export = function render () {' + output.render + '}',es2015opts);

            // Write the destination file.
            grunt.file.write(path.dirname(f)+'/render.js', output2015);
          } catch(e) {
            console.log(e);
            err = new Error('Template compiling failed.');
            if (e.message) {
              err.message += '\n' + e.message + '. \n';
              if (e.line) {
                err.message += 'Line ' + e.line + ' in ' + src + '\n';
              }
            }
            err.origError = e;
            grunt.log.warn('Vue template compiling ' + src + ' failed.');
            grunt.fail.warn(err);     
          }
        }
      }
    });
    grunt.log.writeln('Render.js files created from template.html files');
  });
};
