This is a fork of Simon Cast's code. https://github.com/simoncast

The primary change is to include staticRenderFns the render export.

# grunt-vue-template-compiler

This task runs the [vue-template-validator](https://www.npmjs.com/package/vue-template-validator), [vue-template-compiler](https://www.npmjs.com/package/grunt-vue-template-compiler) and then [vue-template-es2015-compiler](https://www.npmjs.com/package/vue-template-es2015-compiler) on Vue HTML files to produce the pre-compile JS functions. These functions can then be substituted for `import: template from ./template.html!text` in the JS files and builder will then merge these into the build.js file, avoiding the need to compile the HTML files at run time.

## Getting Started
This plugin requires Grunt `~0.4.5`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-vue-template-compiler --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-vue-template-compiler');
```

## The "vue_template_compiler" task

### Overview
In your project's Gruntfile, add a section named `vue_template_compiler` to the data object passed into `grunt.initConfig()`.

```Simple Example
grunt.initConfig({
  vue_template_compiler: {
    options: {
      validate: true, //default is true
      validateOnly: false, //default is false
      es2015: true //default is true
    },
    your_target: {
      files : {
        'app/js/**/*.js' : 'app/js/**/template.html' //note that JS files will be named output as render.js
      }
    },
  },
});
```

### Options

#### options.validate
Type: `Boolean`
Default value: `true`

This option tells the task to run the html files through the [vue-template-validator](https://www.npmjs.com/package/vue-template-validator). 

#### options.validateOnly
Type: `Boolean`
Default value: `false`

This option tells the task to run the html files through [vue-template-validator](https://www.npmjs.com/package/vue-template-validator) only and not run the compile steps.

#### options.es2015
Type: `Boolean`
Default value: `true`

This option tells the task to take the complied HTML files and run them through [vue-template-es2015-compiler](https://www.npmjs.com/package/vue-template-es2015-compiler) to take advantage of es2015 features and, more importantly, remove the with(this) inside the render function to make it strict compliant.

## Notes on Usage
Once the render.js files have been created you'll need to use a grunt task like [grunt-string-replace](https://www.npmjs.com/package/grunt-string-replace) to find import statements for the html files and replace them with render.js.

You'll also be able to replace vue.min.js with vue.runtime.min.js in production.

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
0.4.1 Minor formating on read me

0.4.0 Big read me update

0.3.1 Updated the readme with extra details and a usage example.

0.3.0 Added Vue template validator and the ES2015 compiler steps to the task

0.1.1 Minor update

0.1.0 First version

